#include "cam.h"

using namespace cdp;

///////////////////////////////////////////

bool Cam::init(int dev_id)
{
	cam_state = CAM_SAMPLING;

	if(!check() )
	{
		cam_state = CAM_ERR_UNFOUND;
		cout << "cam unfound camera." << endl;
		return false;
	}

	if(pthread_create(&pth_id, NULL, _cam_sample, (void*)this) != 0)
	{
		cam_state = CAM_ERR_THREAD;
		cout << "pthread for cam create error." << endl;
		return false;
	}

	cout << "cam init successfully!" << endl;
	return true;
}

void Cam::quit(void)
{
	cam_state = CAM_ENDING;
	pthread_join(pth_id, NULL);
	cout << "pthread for cam quit successfully!" << endl;
}


///////////////////////////////////////////

bool Cam::check()
{
	VideoCapture cap(0);
	if(!cap.isOpened() )
	{
		cap.release();
		return false;
	}
	cap.release();
	return true;
}

void Cam::get()
{
	//TODO
}

///////////////////////////////////////////

void Cam::setup()
{
	//TODO
}

void Cam::update()
{
	//TODO

	//debug
	//cout << "updating..." << endl;
}

///////////////////////////////////////////

void* Cam::_cam_sample(void* arg)
{
	Cam *_cam = (Cam *)arg;

	_cam->setup();

	while(_cam->cam_state == CAM_SAMPLING)
	{
		_cam->update();	
	}

	return NULL;
}
