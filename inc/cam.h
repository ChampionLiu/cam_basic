#ifndef __CAM_H
#define __CAM_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <pthread.h>

using namespace std;
using namespace cv;

//capture device pool
namespace cdp
{
	enum _Cam_State
	{
		CAM_ERR_UNKOWN	=	-15,
		CAM_ERR_UNFOUND	=	-14,
		CAM_ERR_THREAD	=	-13,
		CAM_ENDING		=	-1,
		CAM_READY		=	0,
		CAM_SAMPLING	=	1
	};

	class Cam
	{
	public:
		Cam(){cam_state = 0;}
		~Cam(){}

		bool init(int dev_id = 0);
		void quit(void);

		virtual bool check();
		virtual void get();

	protected:
		virtual void setup(void);
		virtual void update(void);

	private:
		int cam_state;
		pthread_t pth_id;
		static void* _cam_sample(void* arg);
	};

}

#endif
