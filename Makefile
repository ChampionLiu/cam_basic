TARGET = cam_test

SRCS := $(wildcard ./*.cpp ./src/*.cpp)

OBJS := $(patsubst %cpp,%o,$(SRCS))

CFLAGS = -g -Wall -I/usr/local/include -Iinc -std=c++11

LDFLAGS = -Wl,-rpath,./ -lrt -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_videoio -lpthread

CXX = g++

$(TARGET) : $(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LDFLAGS)

%.o:%.cpp
	$(CXX) $(CFLAGS) -c $< -o $@

.PHONY : clean
clean:
	-rm src/*.o
	-rm ./*.o
