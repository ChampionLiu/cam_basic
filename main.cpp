#include "cam.h"
#include <string>

using namespace cdp;

int main(int argc, char** argv)
{
	Cam cam;
	cam.init();

	bool isQuit = false;
	while(!isQuit)
	{
		string cmd;
		cin >> cmd;

		if(cmd == "q")
		{
			cam.quit();
			cout << "goodbye" << endl;
			isQuit = true;
		}
	}

	return 0;
}
